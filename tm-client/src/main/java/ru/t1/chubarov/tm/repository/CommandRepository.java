package ru.t1.chubarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.chubarov.tm.api.repository.ICommandRepository;
import ru.t1.chubarov.tm.listener.AbstractListener;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

@Repository
public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractListener> mapByArgument = new TreeMap<>();
    @NotNull
    private final Map<String, AbstractListener> mapByName = new TreeMap<>();

    @Override
    public void add(@Nullable final AbstractListener command) {
        if (command == null) return;
        @Nullable final String name = command.getName();
        if (name != null && !name.isEmpty()) mapByName.put(name, command);
        @Nullable final String argument = command.getArgument();
        if (argument != null && !argument.isEmpty()) mapByArgument.put(argument, command);
    }

    @Nullable
    @Override
    public AbstractListener getCommandByArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) return null;
        return mapByArgument.get(argument);
    }

    @Nullable
    @Override
    public AbstractListener getCommandByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return mapByName.get(name);
    }

    @NotNull
    @Override
    public Collection<AbstractListener> getTerminalCommands() {
        return mapByName.values();
    }

    @Override
    @NotNull
    public Iterable<AbstractListener> getCommandWithArgument() {
        return mapByArgument.values();
    }

}
