package ru.t1.chubarov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.dto.request.TaskChangeStatusByIdRequest;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.event.ConsoleEvent;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class TaskChangeStatusByIdListener extends AbstractTaskListener {

    @Override
    @EventListener(condition = "@taskChangeStatusByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[CHANGE TASK STATUS BY ID");
        System.out.println("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS: ");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.toStatus(statusValue);

        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(getToken());
        request.setId(id);
        request.setStatus(status);
        taskEndpoint.changeTaskStatusById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-change-status-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change task status by id.";
    }

}
