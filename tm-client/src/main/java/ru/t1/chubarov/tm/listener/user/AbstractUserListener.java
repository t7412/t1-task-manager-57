package ru.t1.chubarov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.chubarov.tm.api.endpoint.IUserEndpoint;
import ru.t1.chubarov.tm.listener.AbstractListener;
import ru.t1.chubarov.tm.dto.model.UserDTO;

@Component
public abstract class AbstractUserListener extends AbstractListener {

    @NotNull
    @Autowired
    public IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    public IAuthEndpoint authEndpoint;

    @Nullable
    public String getArgument() {
        return null;
    }

    public void showUser(@Nullable final UserDTO user) {
        if (user == null) return;
        System.out.println("ID: " + user.getId());
        System.out.println("  LOGIN: " + user.getLogin());
        System.out.println("  EMAIL: " + user.getEmail());
        System.out.println("  FIRST NAME: " + user.getFirstName());
        System.out.println("  LAST NAME: " + user.getLastName());
        System.out.println("  MIDDLE NAME: " + user.getMiddleName());
    }

}
