package ru.t1.chubarov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.event.ConsoleEvent;
import ru.t1.chubarov.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class CommandListListener extends AbstractSystemListener {
    @Override
    @EventListener(condition = "@commandListListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[COMMANDS]");
        @Nullable final Collection<AbstractListener> commands = commandService.getTerminalCommands();
        for (@Nullable final AbstractListener command : commands) {
            if (command == null) continue;
            @Nullable final String name = getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "commands";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show command list.";
    }
}
