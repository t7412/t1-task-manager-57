package ru.t1.chubarov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.property.IDatabaseProperty;
import ru.t1.chubarov.tm.api.repository.model.ISessionModelRepository;
import ru.t1.chubarov.tm.api.repository.model.IUserModelRepository;
import ru.t1.chubarov.tm.api.service.IConnectionService;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.marker.UnitCategory;
import ru.t1.chubarov.tm.model.Session;
import ru.t1.chubarov.tm.model.User;
import ru.t1.chubarov.tm.repository.model.SessionRepository;
import ru.t1.chubarov.tm.repository.model.UserRepository;
import ru.t1.chubarov.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

//@Category(UnitCategory.class)
//public class SessionRepositoryTest {
//
//    private static final int NUMBER_OF_ENTRIES = 2;
//
//    @NotNull
//    private final String userIdFirstSession = UUID.randomUUID().toString();
//
//    @NotNull
//    private final String userIdSecondSession = UUID.randomUUID().toString();
//
//    @NotNull
//    private final String firstSessionId = UUID.randomUUID().toString();
//
//    @NotNull
//    private List<Session> sessionList;
//
//    @NotNull
//    private final String userIdFirst = UUID.randomUUID().toString();
//
//    @NotNull
//    private final User userFirst = new User();
//
//    @NotNull
//    private final IDatabaseProperty databaseProperty = new PropertyService();
//
//    @NotNull
//    private final IConnectionService connectionService = new ConnectionService(databaseProperty);
//
//    @NotNull
//    private final EntityManager entityManager = connectionService.getEntityManager();
//
//    @NotNull
//    private final ISessionModelRepository sessionRepository = new SessionRepository(entityManager);
//
//    @NotNull
//    private final IUserModelRepository repositoryUser = new UserRepository(entityManager);
//
//    @SneakyThrows
//    @Before
//    public void initRepository() {
//        entityManager.getTransaction().begin();
//        userFirst.setId(userIdFirst);
//        userFirst.setRole(Role.USUAL.toString());
//        userFirst.setLocked(false);
//        repositoryUser.add(userFirst);
//        entityManager.getTransaction().commit();
//        entityManager.getTransaction().begin();
//        sessionList = new ArrayList<>();
//        @NotNull final Session session1 = new Session();
//        session1.setUser(userFirst);
//        session1.setDate(new Date());
//        session1.setId(firstSessionId);
//        sessionRepository.add(session1);
//        sessionList.add(session1);
//        @NotNull final Session session2 = new Session();
//        session2.setUser(userFirst);
//        session2.setDate(new Date());
//        sessionRepository.add(session2);
//        sessionList.add(session2);
//        entityManager.getTransaction().commit();
//    }
//
//    @After
//    public void finish() {
//        sessionList.clear();
//        try {
//            entityManager.getTransaction().begin();
//            sessionRepository.clear();
//            entityManager.getTransaction().commit();
//        } catch (@NotNull final Exception e) {
//            throw e;
//        }
//        entityManager.close();
//    }
//
//    @SneakyThrows
//    @Test
//    public void testAdd() {
//        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
//        @NotNull final Session session1 = new Session();
//        session1.setUser(userFirst);
//        session1.setDate(new Date());
//        entityManager.getTransaction().begin();
//        sessionRepository.add(session1);
//        entityManager.getTransaction().commit();
//        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, sessionRepository.getSize());
//        @NotNull final Session session2 = new Session();
//        session2.setUser(userFirst);
//        session2.setDate(new Date());
//        entityManager.getTransaction().begin();
//        sessionRepository.add(session2);
//        entityManager.getTransaction().commit();
//        Assert.assertEquals(NUMBER_OF_ENTRIES + 2, sessionRepository.getSize());
//    }
//
//    @SneakyThrows
//    @Test
//    public void testRemove() {
//        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
//        @NotNull final Session session = sessionRepository.findOneById(firstSessionId);
//        entityManager.getTransaction().begin();
//        sessionRepository.remove(session);
//        entityManager.getTransaction().commit();
//        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, sessionRepository.getSize());
//    }
//
//    @SneakyThrows
//    @Test
//    public void testRemoveOneById() {
//        entityManager.getTransaction().begin();
//        sessionRepository.removeOneById(userIdFirst, "empty_sessionId");
//        entityManager.getTransaction().commit();
//        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
//        entityManager.getTransaction().begin();
//        sessionRepository.removeOneById(userIdFirst, firstSessionId);
//        entityManager.getTransaction().commit();
//        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, sessionRepository.getSize());
//    }
//
//    @SneakyThrows
//    @Test
//    public void testExistsById() {
//        @NotNull Boolean cnt = sessionRepository.existsById(userIdFirst, firstSessionId);
//        Assert.assertTrue(cnt);
//        Assert.assertTrue(sessionRepository.existsById(userIdFirst, sessionList.get(1).getId()));
//        cnt = sessionRepository.existsById(userIdFirst, "empty_id");
//        Assert.assertFalse(cnt);
//    }
//
//    @SneakyThrows
//    @Test
//    public void testFindOneById() {
//        @NotNull final Session session = sessionRepository.findOneById(firstSessionId);
//        Assert.assertNotNull(sessionRepository.findOneById(firstSessionId));
//    }
//
//    @SneakyThrows
//    @Test
//    public void testFindOneByIdNegative() {
//        Assert.assertNull(sessionRepository.findOneById("empty_id"));
//    }
//
//    @Test
//    public void testFindOAll() throws Exception {
//        @NotNull final List<Session> SessionList = sessionRepository.findAll();
//        Assert.assertEquals(2, SessionList.size());
//        @NotNull final List<Session> actualSessionList = sessionRepository.findAllByUser(userIdFirst);
//        Assert.assertEquals(2, actualSessionList.size());
//    }
//
//    @SneakyThrows
//    @Test
//    public void testGetSizeForUser() {
//        Assert.assertEquals(2, sessionRepository.getSizeByUser(userIdFirst));
//    }
//
//}
