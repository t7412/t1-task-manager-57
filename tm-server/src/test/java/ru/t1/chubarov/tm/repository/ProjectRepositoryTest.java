package ru.t1.chubarov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.property.IDatabaseProperty;
import ru.t1.chubarov.tm.api.repository.model.IProjectModelRepository;
import ru.t1.chubarov.tm.api.repository.model.IUserModelRepository;
import ru.t1.chubarov.tm.api.service.IConnectionService;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.marker.UnitCategory;
import ru.t1.chubarov.tm.model.Project;
import ru.t1.chubarov.tm.model.User;
import ru.t1.chubarov.tm.repository.model.ProjectRepository;
import ru.t1.chubarov.tm.repository.model.UserRepository;
import ru.t1.chubarov.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

//@Category(UnitCategory.class)
//public class ProjectRepositoryTest {
//
//    private static final int NUMBER_OF_ENTRIES = 10;
//
//    @NotNull
//    private final String userIdFirst = UUID.randomUUID().toString();
//
//    @NotNull
//    private final String userIdSecond = UUID.randomUUID().toString();
//
//    @NotNull
//    private final User userFirst = new User();
//
//    @NotNull
//    private List<Project> projectList;
//
//    @NotNull
//    private final IDatabaseProperty databaseProperty = new PropertyService();
//
//    @NotNull
//    private final IConnectionService connectionService = new ConnectionService(databaseProperty);
//
//    @NotNull
//    private final EntityManager entityManager = connectionService.getEntityManager();
//
//    @NotNull
//    private final IProjectModelRepository projectRepository = new ProjectRepository(entityManager);
//
//    @NotNull
//    private final IUserModelRepository repositoryUser = new UserRepository(entityManager);
//
//    @SneakyThrows
//    @Before
//    public void initRepository() {
//        try {
//            entityManager.getTransaction().begin();
//            userFirst.setId(userIdFirst);
//            userFirst.setRole(Role.USUAL.toString());
//            userFirst.setLocked(false);
//            repositoryUser.add(userFirst);
//            @NotNull final User userSecond = new User();
//            userSecond.setId(userIdSecond);
//            userSecond.setRole(Role.USUAL.toString());
//            userSecond.setLocked(false);
//            repositoryUser.add(userSecond);
//            entityManager.getTransaction().commit();
//            entityManager.getTransaction().begin();
//            projectList = new ArrayList<>();
//
//            for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
//                @NotNull final Project project = new Project();
//                project.setName("project_" + i);
//                project.setDescription("project description_" + i);
//                if (i <= 5) {
//                    project.setUser(userFirst);
//                } else project.setUser(userSecond);
//                projectRepository.add(project);
//                projectList.add(project);
//            }
//            entityManager.getTransaction().commit();
//        } catch (@NotNull final Exception e) {
//            throw e;
//        }
//
//    }
//
//    @After
//    public void finish() {
//        projectList.clear();
//        try {
//            entityManager.getTransaction().begin();
//            projectRepository.clear();
//            entityManager.getTransaction().commit();
//            entityManager.getTransaction().begin();
//            repositoryUser.clear();
//            entityManager.getTransaction().commit();
//        } catch (@NotNull final Exception e) {
//            throw e;
//        }
//        entityManager.close();
//    }
//
//    @SneakyThrows
//    @Test
//    public void testAdd() {
//        int numberOfProject = NUMBER_OF_ENTRIES + 1;
//        @NotNull final String userId = UUID.randomUUID().toString();
//        @NotNull final String name = "Test Project";
//        @NotNull final String description = "Test Project Description";
//        @NotNull Project project = new Project();
//        @NotNull String projectId = UUID.randomUUID().toString();
//        entityManager.getTransaction().begin();
//        project.setId(projectId);
//        project.setName(name);
//        project.setDescription(description);
//        project.setUser(userFirst);
//        projectRepository.add(project);
//        entityManager.getTransaction().commit();
//        Assert.assertEquals(numberOfProject, projectRepository.getSize());
//        @Nullable final Project actualproject = projectRepository.findOneById(projectId);
//        Assert.assertNotNull(actualproject);
//        Assert.assertEquals(projectId, actualproject.getId());
//        Assert.assertEquals(userFirst.getId(), actualproject.getUser().getId());
//        Assert.assertEquals(name, actualproject.getName());
//        Assert.assertEquals(description, actualproject.getDescription());
//    }
//
//    @SneakyThrows
//    @Test
//    public void testRemoveAllbyUser() {
//        entityManager.getTransaction().begin();
//        projectRepository.removeAll(userIdFirst);
//        entityManager.getTransaction().commit();
//        Assert.assertEquals(5, projectRepository.getSize());
//    }
//
//    @Test
//    public void testFindAllbyUser() {
//        @NotNull final List<Project> actualProjectList = projectRepository.findAllByUser(userIdFirst);
//        Assert.assertEquals(5, actualProjectList.size());
//    }
//
//    @Test
//    public void testFindAll() {
//        @NotNull final List<Project> actualProjectList = projectRepository.findAll();
//        Assert.assertEquals(10, actualProjectList.size());
//    }
//
//    @SneakyThrows
//    @Test
//    public void testFindOneById() {
//        int numberOfProject = NUMBER_OF_ENTRIES + 1;
//        @NotNull final String userId = UUID.randomUUID().toString();
//        @NotNull final String name = "Test Project";
//        @NotNull Project project = new Project();
//        @NotNull String projectId = UUID.randomUUID().toString();
//        project.setId(projectId);
//        project.setName(name);
//        project.setUser(userFirst);
//        entityManager.getTransaction().begin();
//        projectRepository.add(project);
//        entityManager.getTransaction().commit();
//        Assert.assertEquals(numberOfProject, projectRepository.getSize());
//        Assert.assertNotNull(project);
//        Assert.assertEquals(projectId, projectRepository.findOneById(projectId).getId());
//    }
//
//    @SneakyThrows
//    @Test
//    public void testRemoveOne() {
//        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
//        entityManager.getTransaction().begin();
//        projectRepository.remove(projectList.get(1));
//        entityManager.getTransaction().commit();
//        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, projectRepository.getSize());
//        Assert.assertEquals(4, projectRepository.getSizeByUser(userIdFirst));
//    }
//
//    @SneakyThrows
//    @Test
//    public void testRemoveOneByUser() {
//        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
//        entityManager.getTransaction().begin();
//        projectRepository.remove(userIdFirst, projectList.get(1));
//        entityManager.getTransaction().commit();
//        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, projectRepository.getSize());
//        Assert.assertEquals(4, projectRepository.getSizeByUser(userIdFirst));
//    }
//
//    @SneakyThrows
//    @Test
//    public void testRemoveOneById() {
//        int numberOfProject = NUMBER_OF_ENTRIES + 1;
//        @NotNull final Project project = new Project();
//        @NotNull String projectId = UUID.randomUUID().toString();
//        project.setId(projectId);
//        project.setName("Test Project");
//        project.setUser(userFirst);
//        entityManager.getTransaction().begin();
//        projectRepository.add(project);
//        entityManager.getTransaction().commit();
//        Assert.assertEquals(numberOfProject, projectRepository.getSize());
//        @NotNull final Project actualProject = projectRepository.findOneById(projectId);
//        Assert.assertNotNull(actualProject);
//
//        entityManager.getTransaction().begin();
//        projectRepository.removeOneById(userIdFirst, "fail_test_id");
//        entityManager.getTransaction().commit();
//        Assert.assertEquals(numberOfProject, projectRepository.getSize());
//
//        entityManager.getTransaction().begin();
//        projectRepository.removeOneById(userIdFirst, actualProject.getId());
//        entityManager.getTransaction().commit();
//        Assert.assertEquals(numberOfProject - 1, projectRepository.getSize());
//    }
//
//    @SneakyThrows
//    @Test
//    public void testExistById() {
//        @NotNull final String name = "Test Project";
//        @NotNull final Project project = new Project();
//        @NotNull String projectId = UUID.randomUUID().toString();
//        project.setId(projectId);
//        project.setName(name);
//        project.setUser(userFirst);
//        entityManager.getTransaction().begin();
//        projectRepository.add(project);
//        entityManager.getTransaction().commit();
//        Assert.assertEquals(true, projectRepository.existsById(userIdFirst, projectId));
//        Assert.assertEquals(false, projectRepository.existsById(userIdFirst, "1111"));
//        Assert.assertEquals(false, projectRepository.existsById("userId-123-4", projectId));
//    }
//
//    @SneakyThrows
//    @Test
//    public void testUpdate() {
//        int numberOfProject = NUMBER_OF_ENTRIES + 1;
//        @NotNull String name = "Test Project";
//        @NotNull String description = "Test Project Description";
//        @NotNull Project project = new Project();
//        @NotNull String projectId = UUID.randomUUID().toString();
//        project.setId(projectId);
//        project.setName(name);
//        project.setDescription(description);
//        project.setUser(userFirst);
//        entityManager.getTransaction().begin();
//        projectRepository.add(project);
//        entityManager.getTransaction().commit();
//        Assert.assertEquals(numberOfProject, projectRepository.getSize());
//
//        name = "NewProject";
//        description = "Update Project desc";
//        project.setName(name);
//        project.setDescription(description);
//        project.setUser(userFirst);
//        projectRepository.update(project);
//        @Nullable final Project actualproject = projectRepository.findOneById(projectId);
//        Assert.assertNotNull(actualproject);
//        Assert.assertEquals(projectId, actualproject.getId());
//        Assert.assertEquals(userIdFirst, actualproject.getUser().getId());
//        Assert.assertEquals(name, actualproject.getName());
//        Assert.assertEquals(description, actualproject.getDescription());
//    }
//
//}
