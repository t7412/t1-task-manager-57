package ru.t1.chubarov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.property.IDatabaseProperty;
import ru.t1.chubarov.tm.api.repository.model.IUserModelRepository;
import ru.t1.chubarov.tm.api.service.IConnectionService;
import ru.t1.chubarov.tm.api.service.IPropertyService;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.marker.UnitCategory;
import ru.t1.chubarov.tm.model.User;
import ru.t1.chubarov.tm.repository.model.UserRepository;
import ru.t1.chubarov.tm.service.PropertyService;
import ru.t1.chubarov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.UUID;

//@Category(UnitCategory.class)
//public class UserRepositoryTest {
//
//    private static final int NUMBER_OF_ENTRIES = 1;
//
//    @NotNull
//    private IPropertyService propertyService;
//
//    @NotNull
//    private final IDatabaseProperty databaseProperty = new PropertyService();
//
//    @NotNull
//    private final IConnectionService connectionService = new ConnectionService(databaseProperty);
//
//    @NotNull
//    private final EntityManager entityManager = connectionService.getEntityManager();
//
//    @NotNull
//    private final IUserModelRepository userRepository = new UserRepository(entityManager);
//
//    @NotNull
//    private final String userIdFirst = UUID.randomUUID().toString();
//
//    @NotNull
//    private final String userLogin = "testlogin";
//
//    @NotNull
//    private final String userEmail = "Test@mail.test";
//
//    @SneakyThrows
//    @Before
//    public void initRepository() {
//        propertyService = new PropertyService();
//        entityManager.getTransaction().begin();
//        userRepository.clear();
//        entityManager.getTransaction().commit();
//
//        entityManager.getTransaction().begin();
//        @NotNull final User user = new User();
//        user.setId(userIdFirst);
//        user.setLogin(userLogin);
//        user.setEmail(userEmail);
//        user.setPasswordHash(HashUtil.salt(propertyService, "password"));
//        user.setRole(Role.USUAL.toString());
//        userRepository.add(user);
//        entityManager.getTransaction().commit();
//    }
//
//    @After
//    public void finish() {
//        try {
//            entityManager.getTransaction().begin();
//            @NotNull final User user = new User();
//            user.setLogin(userLogin);
//            userRepository.remove(user);
//            userRepository.clear();
//            entityManager.getTransaction().commit();
//        } catch (@NotNull final Exception e) {
//            throw e;
//        } finally {
//            entityManager.close();
//        }
//    }
//
//    @SneakyThrows
//    @Test
//    public void testFindAll() {
//        List<User> users = userRepository.findAll();
//        Assert.assertEquals(1, users.size());
//    }
//
//    @SneakyThrows
//    @Test
//    public void testFindOneById() {
//        User user = userRepository.findOneById(userIdFirst);
//        Assert.assertEquals(userLogin, user.getLogin());
//    }
//
//    @SneakyThrows
//    @Test
//    public void testFindByLogin() {
//        User user = userRepository.findByLogin(userLogin);
//        Assert.assertEquals(userLogin, user.getLogin());
//    }
//
//    @SneakyThrows
//    @Test
//    public void testfindByEmail() {
//        Assert.assertEquals(userLogin, userRepository.findByEmail(userEmail).getLogin());
//    }
//
//    @SneakyThrows
//    @Test
//    public void testisLoginExist() {
//        Assert.assertTrue(userRepository.isLoginExist(userLogin));
//        Assert.assertFalse(userRepository.isLoginExist("Login"));
//    }
//
//    @SneakyThrows
//    @Test
//    public void testisEmailExist() {
//        Assert.assertTrue(userRepository.isEmailExist(userEmail));
//        Assert.assertFalse(userRepository.isEmailExist("user@Email.org"));
//    }
//
//    @SneakyThrows
//    @Test
//    public void testProfile() {
//        @NotNull final User user = userRepository.findByLogin(userLogin);
//        @NotNull final String userId = user.getId();
//        @NotNull final String newLogin = "new_login";
//        @NotNull final String newPassword = "new_password";
//        user.setFirstName("Ivanov");
//        user.setLastName("Piter");
//        user.setMiddleName("Vasilech");
//        user.setLogin(newLogin);
//        user.setPasswordHash(HashUtil.salt(propertyService, newPassword));
//        entityManager.getTransaction().begin();
//        userRepository.update(user);
//        entityManager.getTransaction().commit();
//        Assert.assertNull(userRepository.findByLogin(userLogin));
//        @NotNull final User updated_user = userRepository.findByLogin(newLogin);
//        Assert.assertEquals(userId, updated_user.getId());
//        Assert.assertEquals("Ivanov", updated_user.getFirstName());
//        Assert.assertEquals("Piter", updated_user.getLastName());
//        Assert.assertEquals("Vasilech", updated_user.getMiddleName());
//        Assert.assertEquals(HashUtil.salt(propertyService, newPassword), updated_user.getPasswordHash());
//    }
//
//    @SneakyThrows
//    @Test
//    public void testSetLock() {
//        @NotNull final User user = userRepository.findByLogin(userLogin);
//        Assert.assertFalse(userRepository.findByLogin(userLogin).getLocked());
//        user.setLocked(true);
//        entityManager.getTransaction().begin();
//        userRepository.update(user);
//        entityManager.getTransaction().commit();
//        Assert.assertTrue(userRepository.findByLogin(userLogin).getLocked());
//        user.setLocked(false);
//        entityManager.getTransaction().begin();
//        userRepository.update(user);
//        entityManager.getTransaction().commit();
//        Assert.assertFalse(userRepository.findByLogin(userLogin).getLocked());
//    }
//
//    @SneakyThrows
//    @Test
//    public void testAdd() {
//        try {
//            Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
//            entityManager.getTransaction().begin();
//            userRepository.add(new User());
//            entityManager.getTransaction().commit();
//            Assert.assertEquals(NUMBER_OF_ENTRIES + 1, userRepository.getSize());
//        } catch (@NotNull final Exception e) {
//            entityManager.getTransaction().rollback();
//            throw e;
//        }
//    }
//
//    @SneakyThrows
//    @Test
//    public void testClear() {
//        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
//        entityManager.getTransaction().begin();
//        userRepository.clear();
//        entityManager.getTransaction().commit();
//        Assert.assertEquals(0, userRepository.getSize());
//    }
//
//    @SneakyThrows
//    @Test
//    public void testRemove() {
//        @NotNull final User user_empty = userRepository.findByLogin("emptyLogin");
//        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
//        @NotNull final User user = userRepository.findByLogin(userLogin);
//        entityManager.getTransaction().begin();
//        userRepository.remove(user);
//        entityManager.getTransaction().commit();
//        Assert.assertEquals(0, userRepository.getSize());
//    }
//
//}
