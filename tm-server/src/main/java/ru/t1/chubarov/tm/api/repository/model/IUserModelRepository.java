package ru.t1.chubarov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.model.User;

public interface IUserModelRepository extends IModelRepository<User> {

    @Nullable
    User findByLogin(@NotNull String id);

    @Nullable
    User findByEmail(@NotNull String id);

    void removeOneById(@Nullable String id);

    @NotNull
    Boolean isLoginExist(@NotNull String login);

    @NotNull
    Boolean isEmailExist(@NotNull String mail);

}
