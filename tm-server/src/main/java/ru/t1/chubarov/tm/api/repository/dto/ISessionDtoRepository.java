package ru.t1.chubarov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionDtoRepository extends IDtoRepository<SessionDTO>{

    @Nullable
    List<SessionDTO> findAll();

    @Nullable
    List<SessionDTO> findAllByUser(@Nullable String userId);

    @Nullable
    SessionDTO findOneById(@NotNull String id);

    @Nullable
    SessionDTO findOneByIdByUser(@Nullable String userId, @Nullable String id);

    void clear();

    void removeAll(@Nullable String userId);

    void remove(@Nullable String userId, @NotNull SessionDTO model);

    void removeOneById(@Nullable String userId, @Nullable String id);

    int getSize();

    int getSizeByUser(@Nullable String userId);

    @NotNull
    Boolean existsById(@Nullable String userId, @Nullable String id);

}
