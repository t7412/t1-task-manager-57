package ru.t1.chubarov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.exception.AbstractException;

@Component
public interface IProjectTaskDtoService {

    void bindTaskToProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) throws Exception;

    void removeProjectById(@NotNull String userId, @NotNull String projectId) throws Exception;

    void unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) throws AbstractException, Exception;

}
