package ru.t1.chubarov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.chubarov.tm.api.service.IPropertyService;

import java.io.IOException;
import java.util.Properties;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

        @NotNull
        public static final String APPLICATION_VERSION_KEY = "buildNumber";

        @NotNull
        public static final String AUTHOR_EMAIL_KEY = "email";

        @NotNull
        public static final String AUTHOR_NAME_KEY = "developer";

        @Value("#{environment['password.iteration']}")
        private Integer passwordIteration;

        @Value("#{environment['password.secret']}")
        private String passwordSecret;

        @Value("#{environment['server.port']}")
        private Integer serverPort;

        @Value("#{environment['server.host']}")
        private String serverHost;

        @Value("#{environment['session.key']}")
        private String sessionKey;

        @Value("#{environment['session.timeout']}")
        private Integer sessionTimeout;

        @Value("#{environment['database.username']}")
        private String databaseUsername;

        @Value("#{environment['database.password']}")
        private String databasePassword;

        @Value("#{environment['database.url']}")
        private String databaseUrl;

        @Value("#{environment['database.driver']}")
        private String databaseDriver;

        @Value("#{environment['database.dialect']}")
        private String databaseDialect;

        @Value("#{environment['database.hbm2ddl_auto']}")
        private String databaseHbm2DDL_Auto;

        @Value("#{environment['database.show_sql']}")
        private String databaseShowSql;

        @Value("#{environment['database.cache.use_second_level_cache']}")
        private String databaseUseSecond;

        @Value("#{environment['database.cache.use_query_cache']}")
        private String databaseUseQueryCache;

        @Value("#{environment['database.cache.use_minimal_puts']}")
        private String databaseMinimalPuts;

        @Value("#{environment['database.cache.region_prefix']}")
        private String databaseRegionPrefix;

        @Value("#{environment['database.cache.region.factory_class']}")
        private String databaseRegionFactory;

        @Value("#{environment['database.cache.provider_configuration_file_resource_path']}")
        private String databaseProviderConfig;

        @Override
        @NotNull
        public String getApplicationVersion() {
            return Manifests.read(APPLICATION_VERSION_KEY);
        }

        @Override
        @NotNull
        public String getAuthorEmail() {
            return Manifests.read(AUTHOR_EMAIL_KEY);
        }

        @Override
        @NotNull
        public String getAuthorName() {
            return Manifests.read(AUTHOR_NAME_KEY);
        }

        @Override
        @NotNull
        public String getServerPortStr() {
            return serverPort.toString();
        }

        @Override
        @NotNull
        public String getSessionKey(@NotNull final String token) {
            return sessionKey;
        }

}
