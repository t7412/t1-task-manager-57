package ru.t1.chubarov.tm.repository.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.chubarov.tm.api.repository.dto.IDtoRepository;
import ru.t1.chubarov.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.List;

@Getter
@Repository
@Scope("prototype")
public abstract class AbstractDtoRepository<M extends AbstractModelDTO> implements IDtoRepository<M> {

    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Nullable
    @Override
    public abstract List<M> findAll();

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public abstract M findOneById(@NotNull final String id);

    @Override
    public abstract void clear();

    @Override
    public void remove(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public abstract int getSize();

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

}
