package ru.t1.chubarov.tm.repository.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.chubarov.tm.api.repository.model.IModelRepository;
import ru.t1.chubarov.tm.model.AbstractModel;

import javax.persistence.EntityManager;

@Getter
public abstract class AbstractRepository<M extends AbstractModel> implements IModelRepository<M> {

    @NotNull
    @Autowired
    public EntityManager entityManager;

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

}
