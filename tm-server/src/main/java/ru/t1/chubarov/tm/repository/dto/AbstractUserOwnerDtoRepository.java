package ru.t1.chubarov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.chubarov.tm.api.repository.dto.IUserOwnerDtoRepository;
import ru.t1.chubarov.tm.dto.model.AbstractUserOwnerModelDTO;

import java.util.List;

@Repository
@Scope("prototype")
public abstract class AbstractUserOwnerDtoRepository<M extends AbstractUserOwnerModelDTO> extends AbstractDtoRepository<M> implements IUserOwnerDtoRepository<M> {

    @Override
    public void add(@Nullable final String userId, @NotNull final M model) {
        if (userId != null) {
            model.setUserId(userId);
            add(model);
        }
    }

    @Nullable
    @Override
    public abstract List<M> findAllByUser(@Nullable final String userId);

    @Nullable
    @Override
    public abstract M findOneByIdByUser(@Nullable final String userId, @Nullable final String id);

    @Override
    public abstract void remove(@Nullable final String userId, @NotNull final M model);

    @Override
    public abstract int getSizeByUser(@Nullable final String userId);

    @Override
    public abstract void removeOneById(@Nullable final String userId, @Nullable final String id);

    @Override
    public abstract void removeAll(@Nullable final String userId);

    @NotNull
    @Override
    public abstract Boolean existsById(@Nullable final String userId, @Nullable final String id);

}
