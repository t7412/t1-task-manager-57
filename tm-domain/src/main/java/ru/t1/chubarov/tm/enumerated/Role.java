package ru.t1.chubarov.tm.enumerated;

import org.jetbrains.annotations.Nullable;

public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    @Nullable
    private final String displayName;

    Role(@Nullable String displayName) {
        this.displayName = displayName;
    }

    @Nullable
    public String getDisplayName() {
        return displayName;
    }

}
