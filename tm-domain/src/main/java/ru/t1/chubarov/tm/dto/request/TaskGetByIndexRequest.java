package ru.t1.chubarov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskGetByIndexRequest extends AbstractUserRequest {

    @Nullable
    private String name;

    @Nullable
    private Integer index;

    public TaskGetByIndexRequest(@Nullable final String token) {
        super(token);
    }

}
